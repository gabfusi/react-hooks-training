import React, {useState, useEffect} from 'react';
import logo from './logo.svg';
import './App.css';
import Counter from "./components/Counter";
import LoginForm from "./components/LoginForm";
import RandomList from "./components/RandomList";
import DispatcherExample from "./components/DispatcherExample";
import Example from "./components/Example";

function App() {
  
  const [counterMounted, setCounterMounted] = useState(true);
  const [exampleMounted, setExampleMounted] = useState(true);
  
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo"/>
        
        
        <pre>-------- - -  - -- ------ - - - - ---------</pre>
        {exampleMounted && <Example/>}
        <pre>-------- - -  - -- ------ - - - - ---------</pre>
        <div>
          <button onClick={() => setExampleMounted(true)}>Mount Example</button>
          <button onClick={() => setExampleMounted(false)}>Un-Mount Example</button>
        </div>
        
        <pre>-------- - -  - -- ------ - - - - ---------</pre>
        <pre>-------- - -  - -- ------ - - - - ---------</pre>
        <pre>-------- - -  - -- ------ - - - - ---------</pre>
        <pre>-------- - -  - -- ------ - - - - ---------</pre>
        <pre>-------- - -  - -- ------ - - - - ---------</pre>
        <pre>-------- - -  - -- ------ - - - - ---------</pre>
        <pre>-------- - -  - -- ------ - - - - ---------</pre>
        
        {counterMounted && <Counter/>}
        <pre>-------- - -  - -- ------ - - - - ---------</pre>
        <div>
          <button onClick={() => setCounterMounted(true)}>Mount Counter</button>
          <button onClick={() => setCounterMounted(false)}>Un-Mount Counter</button>
        </div>
        <pre>-------- - -  - -- ------ - - - - ---------</pre>
        <pre>-------- - -  - -- ------ - - - - ---------</pre>
        <pre>-------- - -  - -- ------ - - - - ---------</pre>
        <pre>-------- - -  - -- ------ - - - - ---------</pre>
        <pre>-------- - -  - -- ------ - - - - ---------</pre>
        <pre>-------- - -  - -- ------ - - - - ---------</pre>
  
        <DispatcherExample />
      
      </header>
    
    </div>
  );
}

export default App;
