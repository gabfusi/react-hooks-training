import React, {
  useState,
  useEffect,
  useRef,
  useMemo,
  useLayoutEffect,
  useDebugValue
} from 'react';

function computeExpensiveValue(a, b) {
  return 1000*(a+10)*(b+10);
}

export default function Example() {
  console.log('eg: start/render ------------------');
  
  const [count, setCount] = useState(0);
  const [isOnline, setIsOnline] = useState(false);
  const [rows, setRows] = useState(() => {
    console.log("eg: Called only during the first render,\n" +
      "The returned value will be used only during the first render");
    return 0;
  });
  
  // Remember that the function passed to useMemo runs during rendering.
  // Don’t do anything there that you wouldn’t normally do while rendering.
  // For example, side effects belong in useEffect, not useMemo.
  const expensiveNumber = useMemo(() => {
    // useMemo allows to only calculate the `expensiveNumber`
    // when `count` or `rows` change
    // `expensiveNumber` can be used in the render function === during the execution of the function component.
    const exp = computeExpensiveValue(count, rows);
    console.log(`eg: calculated expensiveNumber (${exp}), due to count or rows change`);
    return exp
  }, [count, rows]);
  
  // ⚠️ IntersectionObserver is created on every render
  const badRef = useRef(new IntersectionObserver(() => {}));
  
  const goodRef = useRef(null);
  // ✅ IntersectionObserver is created lazily once
  function getObserver() {
    if (goodRef.current === null) {
      goodRef.current = new IntersectionObserver(() => {});
    }
    return goodRef.current;
  }
  
  // Show a label in DevTools next to this Hook
  // e.g. "FriendStatus: Online"
  useDebugValue(isOnline ? 'Online' : 'Offline');
  
  /**
   * useEffect:
   * All the following are delayed after
   * this function (function component) returns
   */
  
  // equals to componentDidMount
  useEffect(() => {
    console.log('eg: component mount');
  
    // When you need it, call getObserver()
    getObserver();
  
    // equals to componentDidUnmount
    return () => {
      console.log('eg: component unmount');
    }
  }, []);
  
  // triggered only when count changes
  useEffect(() => {
    console.log('eg: count incremented ' + count)
  }, [count]);
  
  // triggered only when rows changes
  useEffect(() => {
    console.log('eg: rows incremented ' + rows)
  }, [rows]);
  
  // triggered when count or rows change
  useEffect(() => {
    console.log('eg: rows or count incremented ' + (count+rows))
  }, [count, rows]);
  
  // triggered when isOnline value changes
  useEffect(() => {
    console.log('eg: isOnline? ', isOnline)
  }, [isOnline]);
  
  /**
   * useLayoutEffect:
   * it will fire after the component renders
   * (after all DOM mutations)
   */
  
  // The signature is identical to useEffect,
  // but it fires synchronously after all DOM mutations.
  // Use this to read layout from the DOM and synchronously re-render.
  // Updates scheduled inside useLayoutEffect will be flushed synchronously,
  // before the browser has a chance to paint.
  useLayoutEffect(() => {
    console.log("eg: useLayoutEffect")
  });
  
  
  return (
    <div>
      <button
        style={isOnline ? {
          background: 'green',
          fontSize: '18px'
        } : {}}
        onClick={() => setIsOnline(!isOnline)}>
        {!isOnline ? 'go online' : 'connected'}
      </button>
      <br/>
      <p>You clicked {count} times</p>
      <button onClick={() => setCount(count + 1)}>
        Click me
      </button>
      <br/>
      <p>You added {rows} rows</p>
      <button onClick={() => setRows(rows + 1)}>
        Add rows
      </button>
      <p></p>
      <p>{expensiveNumber} euro, that's a lot!</p>
      
      
    </div>
  );

}