import React, {useState, useEffect} from 'react';

export default function RandomList() {
  const [items, setItems] = useState([]);
  
  console.log('RD: start');
  
  const addItem = () => {
    console.log('RD: addItem');
    setItems([
      ...items,
      {
        value: Math.round(Math.random() * 100)
      }
    ]);
  };
  
  // after render
  useEffect(() => {
    // React guarantees the DOM has been updated by the time it runs the effects.
    console.log('RD: componentDidUpdate');
  });
  
  
  useEffect(() => {
    console.log('RD: componentDidMount');
  }, []);
  
  useEffect(() => {
    console.log('RD: componentDidMount');
  }, []);
  
  console.log('RD: before render');
  
  return (
    <div>
      {console.log('RD: render!')}
      {() => console.log('RD: () => render!')}
      <button onClick={addItem}>Add a number</button>
      <ul>
        {items.map(item => (
          <li key={item.value}>{item.value}</li>
        ))}
      </ul>
    </div>
  );
}