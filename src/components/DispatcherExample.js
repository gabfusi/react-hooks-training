import React, {useState, useReducer, useContext} from 'react';

const FiltersDispatch = React.createContext(null);

const filterGroups = [{
  id: 'pri',
  name: 'Price',
  options: [{
    id: 1,
    name: '< 100 euro'
  }, {
    id: 2,
    name: '< 200 euro'
  }, {
    id: 3,
    name: '< 300 euro'
  }, {
    id: 4,
    name: '< 1000 euro'
  }]
}];

function reducer(state, action) {
  console.log(state, action);
  const currentlySelected = state.selected;
  
  if(action.type === 'click') {
    if(action.isSelected) {
      currentlySelected[action.filterId] = true;
    } else {
      currentlySelected[action.filterId] &&
      (delete currentlySelected[action.filterId]);
    }
  }
  
  return {
    ...state,
    selected: currentlySelected
  }
  
}

export default function DispatcherExample() {
  // Note: `dispatch` won't change between re-renders
  const [state, dispatch] = useReducer(reducer, {
    groups: filterGroups,
    selected: {}
  });
  
  const {groups} = state;
  
  return (
    <FiltersDispatch.Provider value={dispatch}>
      <div>selected: {JSON.stringify(state.selected)}</div>
      {groups && groups.map((g) => (
        <Group key={g.name} {...g}>
          {g.options && g.options.map((o) => (
            <Option key={o.id} parentId={g.id} {...o} />
          ))}
        </Group>
      ))}
    </FiltersDispatch.Provider>
  );
}

function Group(props) {
  // If we want to perform an action, we can get dispatch from context.
  const dispatch = useContext(FiltersDispatch);
  const {name, options} = props;
  const isSelected = !!props.selected;
  
  return (
    <div>
      <h4>{name}</h4>
      <ul
        style={isSelected ? {
          background: 'white'
        } : {}}
      >
        {props.children}
      </ul>
    </div>
  );
}

function Option(props) {
  // If we want to perform an action, we can get dispatch from context.
  const dispatch = useContext(FiltersDispatch);
  const {id, parentId, name} = props;
  const [selected, setSelected] = useState(!!props.selected);
  const filterId = `${parentId}-${id}`;
  
  function handleClick() {
    const isSelected = !selected;
    setSelected(isSelected); // this is an async function
    dispatch({ type: 'click', filterId, isSelected });
  }
  
  return (
    <li>
      <button
        onClick={handleClick}
        style={selected ? {
          background: 'blue',
          color: 'white',
          fontSize: '18px'
        } : {}}
      >{name}</button>
    </li>
  );
}