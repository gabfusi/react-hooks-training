import React from 'react';

export default class Counter extends React.Component {
  
  constructor (props) {
    console.log('Constructor');
    super(props);
    
    this.state = {
      counter: 0
    }
    
    this.increment = () => {
      this.setState({counter: this.state.counter + 1})
    }
  
    this.decrement = () => {
      this.setState({counter: this.state.counter - 1})
    }
  }
  
  componentDidMount() {
    console.log("Component did mount");
    console.log("-------------------");
  }
  
  componentDidUpdate(prevProps, prevState, snapshot) {
    console.log("Component did update");
    console.log("-------------------");
  }
  
  componentWillUnmount(prevProps, prevState, snapshot) {
    console.log("Component did unmount");
    console.log("-------------------");
  }
  
  render () {
    console.log('render');
    
    return (
      <div>
        <button onClick={this.decrement}>Decrement</button>
        <button onClick={this.increment}>Increment</button>
  
        <h2>
          Counter: {this.state.counter}
        </h2>
      </div>
    )
  }
}